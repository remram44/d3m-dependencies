import os
from packaging.specifiers import SpecifierSet
import sys
import tomli
import yaml


def normalize_package(name):
    return name.lower().replace('_', '-')


def main():
    # Load reviews
    with open('reviews.yaml') as fp:
        reviews = list(yaml.safe_load_all(fp))
    package_reviews = {}
    for review in reviews:
        package = review.pop('package')
        package = normalize_package(package)
        package_reviews.setdefault(package, []).append(review)

    # Load dependencies
    with open(os.path.join(sys.argv[1], 'poetry.lock'), 'rb') as fp:
        dependencies = tomli.load(fp)['package']
    dependencies_dict = {normalize_package(dep['name']): dep for dep in dependencies}

    # Load top-level dependencies
    with open(os.path.join(sys.argv[1], 'pyproject.toml'), 'rb') as fp:
        top_level = [
            normalize_package(name)
            for name in tomli.load(fp)['tool']['poetry']['dependencies']
            if name.lower() != 'python'
        ]

    # Find reviews for all dependencies
    has_review = 0
    for dep in dependencies:
        name = normalize_package(dep['name'])
        matching_review = None
        for reviews in package_reviews.get(name, ()):
            for review_version, review in reviews['versions'].items():
                if dep['version'] in SpecifierSet(review_version):
                    if matching_review:
                        print(f"Multiple reviews for {name} {dep['version']}")
                    matching_review = dict(reviews)
                    matching_review.pop('versions')
                    matching_review.update(review)

        if matching_review is None:
            if package_reviews.get(name):
                print(f"Missing review: https://pypi.org/project/{name}/{dep['version']}/ (wrong version)")
                dep['review'] = {'maintained': 'outdated'}
            else:
                print(f"Missing review: https://pypi.org/project/{name}/{dep['version']}/")
            continue

        dep['review'] = matching_review
        has_review += 1

    print(f"{has_review} / {len(dependencies)} packages have a review")

    # Write a graph
    with open('deps.dot', 'w') as fp:
        print('digraph G {', file=fp)

        # Write nodes
        for dep in dependencies:
            name = normalize_package(dep['name'])
            color = 'white'
            if 'review' not in dep:
                color = 'gray'
            elif dep['review']['maintained'] is True:
                color = 'green'
            elif dep['review']['maintained'] == 'outdated':
                color = 'orange'
            elif dep['review']['maintained'] == 'contrib':
                color = 'blue'
            else:
                color = 'red'
            print(f"    \"{name}\" [style=filled,fillcolor=\"{color}\"];", file=fp)

        # Write edges
        for dep in dependencies:
            name = normalize_package(dep['name'])
            for depdep in dep.get('dependencies', {}).keys():
                depdep = normalize_package(depdep)
                print(f"    \"{name}\" -> \"{depdep}\";", file=fp)
        print('}', file=fp)

    # Write HTML tree
    with open('deps.html', 'w') as fp:
        print('<!DOCTYPE html>\n<html><head><title>review.py</title></head><body>', file=fp)
        print('<ul>', file=fp)

        def print_package(name, seen):
            new_seen = set(seen)
            new_seen.add(name)

            dep = dependencies_dict[name]
            if 'review' not in dep:
                color = 'gray'
            elif dep['review']['maintained'] is True:
                color = 'green'
            elif dep['review']['maintained'] == 'outdated':
                color = 'orange'
            elif dep['review']['maintained'] == 'contrib':
                color = 'blue'
            else:
                color = 'red'
            print(f"<li><span style=\"color: {color}\">{name}</span><ul>", file=fp)
            if 'review' not in dep or dep['review'].get('author_team'):
                for depdep in dep.get('dependencies', {}).keys():
                    depdep = normalize_package(depdep)
                    if depdep not in new_seen:
                        print_package(depdep, new_seen)
                    else:
                        print(f"<li><span style=\"text-decoration: underline;\">{depdep}</li>", file=fp)
            print(f"</ul></li>", file=fp)

        for name in top_level:
            print_package(name, set())

        print('</ul>', file=fp)
        print('</body></html>', file=fp)


if __name__ == '__main__':
    main()
